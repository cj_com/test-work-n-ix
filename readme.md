## Installation

1. Load repository files and go to the project directory
2. Install composer https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx and run 'composer install'
3. Open SQL folder and run this file for create DB
4. Create .env file like .env.example and fill you DB connection credentials
5. Run 'php artisan migrate'
6. Run in the command line 'php -S localhost:8000' for starting development server
7. Open browser and go to 'http://localhost:8000' 

### Run unit tests
Open project folder and run '.\vendor\bin\phpunit'