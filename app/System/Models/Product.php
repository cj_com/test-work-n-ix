<?php

namespace App\System\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var string table name
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'description',
        'stock',
        'cost',
        'discontinued'
    ];

    /**
     * Disable timestamps fields
     *
     * @var bool
     */
    public $timestamps = false;
}