<?php

namespace App\System\Foundation\ProductsImport;

use App\System\Foundation\ProductsImport\Detectors\ProductCostDetector;
use App\System\Foundation\ProductsImport\Detectors\ProductDetectorContainer;
use App\System\Foundation\ProductsImport\Detectors\ProductDiscontinuedDetector;
use App\System\Foundation\ProductsImport\Exceptions\DetectExceptions;
use App\System\Foundation\ProductsImport\Exceptions\ImportExceptions;
use App\System\Foundation\ProductsImport\Readers\CSVReader;
use App\System\Models\Product;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;

class ProductsImport
{
    /**
     * @var Product
     */
    protected $model;

    /**
     * @var UploadedFile
     */
    protected $file;

    /**
     * @var array
     */
    protected $content;

    /**
     * @var array
     */
    protected $format = [
        'code',
        'name',
        'description',
        'stock',
        'cost',
        'discontinued'
    ];

    /**
     * ProductsImport constructor.
     *
     * @param UploadedFile $file
     * @param Product $model
     */
    public function __construct(UploadedFile $file, Product $model)
    {
        $this->model = $model;
        $this->file = $file;
    }

    /**
     * Start import process
     */
    public function process()
    {
        $this->convertToArray();

        unset($this->content[0]);

        foreach ($this->content as $rowNumber => $row) {
            $result = $this->detectData($row, $rowNumber);

            if($result) {
                $this->save($result);
            }
        }
    }

    /**
     * Save product in to DB
     *
     * @param ProductType $product
     */
    protected function save(ProductType $product)
    {
        if (!$this->model::where('code', $product->code)->exists()) {
            $this->model::insert($product->toArray());

            Log::info(sprintf(
                'Product with code %s imported successfully!',
                $product->code
            ));
        } else {
            Log::error(sprintf(
                'Product with code %s already exists',
                $product->code
            ));
        }
    }

    /**
     * @param array $row
     * @param $rowNumber
     * @return ProductType
     */
    protected function detectData(array $row, $rowNumber)
    {
        $product = new ProductType();

        foreach ($this->format as $key => $field) {
            $product->$field = $row[$key];
        }

        $detectorContainer = new ProductDetectorContainer();

        $detectorContainer
            ->addDetector(new ProductCostDetector())
            ->addDetector(new ProductDiscontinuedDetector());

        try {
            $detectorContainer->processing($product);
        } catch (DetectExceptions $exception) {
            Log::error(sprintf(
                'Import error in %s row: %s',
                $rowNumber,
                $exception->getMessage()
            ));

            return false;
        }

        return $product;
    }

    /**
     * @throws ImportExceptions
     */
    protected function convertToArray()
    {
        if($this->file->getClientOriginalExtension() == 'csv') {
            $this->content = (new CSVReader($this->file->getRealPath()))->get();
        } else {
            throw new ImportExceptions('Current format not supported!');
        }
    }
}