<?php

namespace App\System\Foundation\ProductsImport;

class ProductType
{
    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var float
     */
    public $cost;

    /**
     * @var
     */
    public $stock;

    /**
     * @var int
     */
    public $discontinued;

    /**
     * Returns an array from all object fields
     *
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
}