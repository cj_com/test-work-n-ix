<?php

namespace App\System\Foundation\ProductsImport\Readers;

abstract class ReadersAbstract
{
    /**
     * @var string
     */
    protected $file;

    /**
     * ReadersInterface constructor.
     *
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Function return file content
     *
     * @return array
     */
    abstract public function get(): array;
}