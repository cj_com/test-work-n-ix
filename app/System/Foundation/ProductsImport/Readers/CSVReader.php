<?php

namespace App\System\Foundation\ProductsImport\Readers;

class CSVReader extends ReadersAbstract
{
    /**
     * @return array
     */
    public function get(): array
    {
        return array_map('str_getcsv', file($this->file));
    }
}