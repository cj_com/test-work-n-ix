<?php

namespace App\System\Foundation\ProductsImport\Detectors;

use App\System\Foundation\ProductsImport\Exceptions\DetectExceptions;
use App\System\Foundation\ProductsImport\ProductType;

class ProductDiscontinuedDetector implements DetectorInterface
{
    /**
     * @param ProductType $product
     * @throws DetectExceptions
     */
    public function processing(ProductType $product)
    {
        $product->discontinued = $product->discontinued == 'yes' ? 1: 0;
    }
}