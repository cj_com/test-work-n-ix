<?php

namespace App\System\Foundation\ProductsImport\Detectors;

use App\System\Foundation\ProductsImport\Exceptions\DetectExceptions;
use App\System\Foundation\ProductsImport\ProductType;

class ProductCostDetector implements DetectorInterface
{
    /**
     * @param ProductType $product
     * @throws DetectExceptions
     */
    public function processing(ProductType $product)
    {
        if(!preg_match('/^\d+(?:\.\d{2})?$/', $product->cost)) {
            throw new DetectExceptions('Price not found!');
        }
    }
}