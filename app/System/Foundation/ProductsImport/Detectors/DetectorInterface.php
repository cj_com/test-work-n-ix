<?php

namespace App\System\Foundation\ProductsImport\Detectors;

use App\System\Foundation\ProductsImport\ProductType;

interface DetectorInterface
{
    public function processing(ProductType $product);
}