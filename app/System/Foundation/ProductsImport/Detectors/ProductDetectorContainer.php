<?php

namespace App\System\Foundation\ProductsImport\Detectors;

use App\System\Foundation\ProductsImport\ProductType;

class ProductDetectorContainer implements DetectorInterface
{
    /**
     * @var DetectorInterface[]
     */
    protected $detectors = [];

    /**
     * @param ProductType $product
     */
    public function processing(ProductType $product)
    {
        foreach ($this->detectors as $detector) {
            if($detector->processing($product)) {
                break;
            }
        }
    }

    /**
     * @param DetectorInterface $detector
     * @return $this
     */
    public function addDetector(DetectorInterface $detector)
    {
        $this->detectors[] = $detector;

        return $this;
    }
}