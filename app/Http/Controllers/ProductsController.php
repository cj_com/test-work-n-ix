<?php

namespace App\Http\Controllers;

use App\System\Models\Product;

class ProductsController extends Controller
{
    const INDEX_PAGE = 'products.index';

    /**
     * Product page
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view(self::INDEX_PAGE, ['products' => Product::get()]);
    }
}
