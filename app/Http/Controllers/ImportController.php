<?php

namespace App\Http\Controllers;

use App\System\Foundation\ProductsImport\Exceptions\ImportExceptions;
use App\System\Foundation\ProductsImport\ProductsImport;
use App\System\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ImportController extends Controller
{
    const INDEX_PAGE = 'import.index';

    /**
     * Import index page
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view(self::INDEX_PAGE);
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'file' => 'required|mimes:csv,txt',
            ]);
        } catch (ValidationException $exception) {
            return $this->importError($exception->getMessage());
        }

        $import = new ProductsImport($request->file('file'), new Product());

        try {
            $import->process();
        } catch (ImportExceptions $exception) {
            return $this->importError($exception->getMessage());
        }

        return redirect()->route('products');
    }

    /**
     * Function return import error
     *
     * @param $message
     * @return \Illuminate\View\View
     */
    protected function importError($message)
    {
        return view(self::INDEX_PAGE, ['errors' => [$message]]);
    }
}
