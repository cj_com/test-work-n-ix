<?php

namespace Tests\App\System\Foundation\ProductsImport\Detectors;

use App\System\Foundation\ProductsImport\Detectors\ProductCostDetector;
use App\System\Foundation\ProductsImport\Detectors\ProductDetectorContainer;
use App\System\Foundation\ProductsImport\Detectors\ProductDiscontinuedDetector;
use App\System\Foundation\ProductsImport\Exceptions\DetectExceptions;
use App\System\Foundation\ProductsImport\ProductType;

class ProductDetectorContainerTest extends \TestCase
{
    /**
     * @param array $data
     * @param array $result
     *
     * @dataProvider productDataProviderPositive
     */
    public function testProcessPositive(array $data, array $result)
    {
        $productDetectorContainer = new ProductDetectorContainer();
        $productDetectorContainer
            ->addDetector(new ProductCostDetector())
            ->addDetector(new ProductDiscontinuedDetector());

        $product = $this->buildProduct($data);

        $processed = $productDetectorContainer->processing($product);

        $this->assertEquals($product, $this->buildProduct($result));
    }

    /**
     * @param array $data
     * @param class $expectedException
     * @param string $expectedExceptionMessage
     *
     * @dataProvider productDataProviderNegative
     */
    public function testProcessNegative($data, $expectedException, $expectedExceptionMessage)
    {
        $productDetectorContainer = new ProductDetectorContainer();
        $productDetectorContainer
            ->addDetector(new ProductCostDetector())
            ->addDetector(new ProductDiscontinuedDetector());

        $product = $this->buildProduct($data);

        $this->expectException($expectedException);
        $this->expectExceptionMessage($expectedExceptionMessage);

        $processed = $productDetectorContainer->processing($product);
    }

    /**
     * @param $data
     * @return ProductType
     */
    protected function buildProduct($data)
    {
        $product = new ProductType();

        $product->code = $data['code'];
        $product->name = $data['name'];
        $product->description = $data['description'];
        $product->stock = $data['stock'];
        $product->cost = $data['cost'];
        $product->discontinued = $data['discontinued'];

        return $product;
    }

    /**
     * Positive Product data provider
     *
     * @return array
     */
    public function productDataProviderPositive()
    {
        return [
            [
                [
                    'code' => 'P0001',
                    'name' => 'TV',
                    'description' => '32” Tv',
                    'stock' => '10',
                    'cost' => '399.99',
                    'discontinued' => 'yes'
                ],
                [
                    'code' => 'P0001',
                    'name' => 'TV',
                    'description' => '32” Tv',
                    'stock' => '10',
                    'cost' => '399.99',
                    'discontinued' => 1
                ]
            ],
            [
                [
                    'code' => 'P0001',
                    'name' => 'TV',
                    'description' => '',
                    'stock' => '',
                    'cost' => '10',
                    'discontinued' => ''
                ],
                [
                    'code' => 'P0001',
                    'name' => 'TV',
                    'description' => '',
                    'stock' => '',
                    'cost' => '10',
                    'discontinued' => 0
                ]
            ],
        ];
    }

    /**
     * Negative Product data provider
     *
     * @return array
     */
    public function productDataProviderNegative()
    {
        return [
            [
                [
                    'code' => 'P0001',
                    'name' => 'TV',
                    'description' => '32” Tv',
                    'stock' => '10',
                    'cost' => '',
                    'discontinued' => 'yes'
                ],
                DetectExceptions::class,
                'Price not found!'
            ],
            [
                [
                    'code' => 'P0001',
                    'name' => 'TV',
                    'description' => '32” Tv',
                    'stock' => '',
                    'cost' => 'dasdas',
                    'discontinued' => 'yes'
                ],
                DetectExceptions::class,
                'Price not found!'
            ]
        ];
    }
}