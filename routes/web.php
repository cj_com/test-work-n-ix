<?php

/**
 * Default home page
 */
$router->get('/', [
    'as' => 'products',
    'uses' => 'ProductsController@index'
]);


$router->get('/import', [
    'as' => 'import',
    'uses' => 'ImportController@index'
]);

$router->post('/import', [
    'as' => 'import.store',
    'uses' => 'ImportController@store'
]);

